LIBOBJS += $(wildcard ../../O.${T_A}/src/.libs/*.o)

include ${EPICS_ENV_PATH}/module.Makefile

SRCPATH=szip-2.1

SOURCES = -none-

HEADERS += ${SRCPATH}/src/szip_adpt.h
HEADERS += ${SRCPATH}/src/szlib.h
HEADERS += ${SRCPATH}/src/ricehdf.h

vpath %.h ../../O.${EPICS_HOST_ARCH}/src
